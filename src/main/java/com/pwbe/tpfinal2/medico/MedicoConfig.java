package com.pwbe.tpfinal2.medico;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class MedicoConfig {
    @Bean
    CommandLineRunner commandLineRunnerMedico(MedicoRepository repository) {
        return args -> {
            Medico luis = new Medico(
                    "98233234",
                    "Luis",
                    "Gimenez",
                    "gimenezlu",
                    "gimenezlu2021",
                    "PEDIATRA"
            );

            Medico juan = new Medico(
                    "74834332",
                    "Juan",
                    "Gomez",
                    "gomezju",
                    "gomezju2021",
                    "CLINICO"
            );

            Medico marta = new Medico(
                    "8472323",
                    "Marta",
                    "Lopez",
                    "lopezma",
                    "lopezma2021",
                    "CARDIOLOGO"
            );

            repository.saveAll(
                    List.of(luis, juan, marta)
            );
        };
    }
}
