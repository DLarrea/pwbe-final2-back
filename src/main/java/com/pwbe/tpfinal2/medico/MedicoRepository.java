package com.pwbe.tpfinal2.medico;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MedicoRepository extends JpaRepository<Medico, Long> {

    @Query("SELECT s FROM Medico s WHERE s.cedula = ?1")
    Optional<Medico> findMedicoByCedula(String cedula);

}
