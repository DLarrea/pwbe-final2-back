package com.pwbe.tpfinal2.medico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class MedicoService {

    private final MedicoRepository medicoRepository;

    @Autowired
    public MedicoService(MedicoRepository medicoRepository) {
        this.medicoRepository = medicoRepository;
    }

    public List<Medico> getMedicos() {
        return medicoRepository.findAll();
    }

    public void addNewMedico(Medico medico) {
        Optional<Medico> medicoOptional = medicoRepository.findMedicoByCedula(medico.getCedula());
        if(medicoOptional.isPresent()){
            throw new IllegalStateException("La cedula ya se encuentra registrada");
        }
        medicoRepository.save(medico);
    }

    public void updateMedico(Medico medico, Long id) throws NoSuchAlgorithmException {

        Medico p = medicoRepository.findById(id).orElseThrow(() -> new IllegalStateException("Medico no encontrado"));

        if( medico.getNombre() != null &&
                medico.getNombre().length() > 0 &&
                !Objects.equals(p.getNombre(), medico.getNombre())
        ){
            p.setNombre(medico.getNombre());
        }

        if( medico.getApellido() != null &&
                medico.getApellido().length() > 0 &&
                !Objects.equals(p.getApellido(), medico.getApellido())
        ){
            p.setApellido(medico.getApellido());
        }

        if( medico.getEmail() != null &&
                medico.getEmail().length() > 0 &&
                !Objects.equals(p.getEmail(), medico.getEmail())
        ){
            p.setEmail(medico.getEmail());
        }

        if( medico.getTelefono() != null &&
                medico.getTelefono().length() > 0 &&
                !Objects.equals(p.getTelefono(), medico.getTelefono())
        ){
            p.setTelefono(medico.getTelefono());
        }

        if( medico.getFechaNacimiento() != null &&
                !Objects.equals(p.getFechaNacimiento(), medico.getFechaNacimiento())
        ){
            p.setFechaNacimiento(medico.getFechaNacimiento());
        }

        if( medico.getEspecialidad() != null &&
                !Objects.equals(p.getEspecialidad(), medico.getEspecialidad())
        ){
            p.setEspecialidad(medico.getEspecialidad());
        }

        if( medico.getUsuario() != null &&
                !Objects.equals(p.getUsuario(), medico.getUsuario())
        ){
            p.setUsuario(medico.getUsuario());
        }

        if( medico.getPassword() != null &&
                !Objects.equals(p.getPassword(), medico.getPassword())
        ){
            p.setPassword(medico.getPassword());
        }

        medicoRepository.save(p);
    }

    public void deleteMedico(Long id) {
        boolean exists = medicoRepository.existsById(id);
        if(!exists){
            throw new IllegalStateException("El medico no existe");
        }
        medicoRepository.deleteById(id);
    }
}
