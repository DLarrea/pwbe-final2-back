package com.pwbe.tpfinal2.medico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@RestController
@RequestMapping(path="api/v1/medico")
public class MedicoController {

    private final MedicoService medicoService;

    @Autowired
    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    @GetMapping
    public List<Medico> getMedicos() {
        return medicoService.getMedicos();
    }

    @PostMapping
    public void registerNewMedico(@RequestBody Medico paciente){
        medicoService.addNewMedico(paciente);
    }

    @PutMapping(path="{medicoId}")
    public void updateMedico(@PathVariable("medicoId") Long id, @RequestBody Medico medico) throws NoSuchAlgorithmException {
        medicoService.updateMedico(medico, id);
    }

    @DeleteMapping(path="{medicoId}")
    public void deleteStudent(@PathVariable("medicoId") Long id){
        medicoService.deleteMedico(id);
    }
}
