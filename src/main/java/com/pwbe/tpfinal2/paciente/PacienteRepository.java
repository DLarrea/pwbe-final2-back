package com.pwbe.tpfinal2.paciente;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PacienteRepository extends JpaRepository<Paciente, Long> {

    @Query("SELECT s FROM Paciente s WHERE s.cedula = ?1")
    Optional<Paciente> findPacienteByCedula(String cedula);
}
