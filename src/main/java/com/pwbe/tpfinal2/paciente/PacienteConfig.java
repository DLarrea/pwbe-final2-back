package com.pwbe.tpfinal2.paciente;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class PacienteConfig {
    @Bean
    CommandLineRunner commandLineRunnerPaciente(PacienteRepository repository) {
        return args -> {
            Paciente diego = new Paciente(
                    "Diego",
                    "Larrea",
                    "5249657"
            );

            Paciente gabriel = new Paciente(
                    "Gabriel",
                    "Barrios",
                    "2342123"
            );

            Paciente maria = new Paciente(
                    "Maria",
                    "Ruiz",
                    "4432653"
            );
            repository.saveAll(
                    List.of(diego, gabriel, maria)
            );
        };
    }
}
