package com.pwbe.tpfinal2.paciente;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="api/v1/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    @Autowired
    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @GetMapping
    public List<Paciente> getPacientes() {
        return pacienteService.getPacientes();
    }

    @PostMapping
    public void registerNewPaciente(@RequestBody Paciente paciente){
        pacienteService.addNewPaciente(paciente);
    }

    @PutMapping(path="{pacienteId}")
    public void updatePaciente(@PathVariable("pacienteId") Long id, @RequestBody Paciente paciente){
        pacienteService.updatePaciente(paciente, id);
    }

    @DeleteMapping(path="{pacienteId}")
    public void deleteStudent(@PathVariable("pacienteId") Long id){
        pacienteService.deleteStudent(id);
    }
}
