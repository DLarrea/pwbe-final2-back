package com.pwbe.tpfinal2.paciente;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class PacienteService {

    private final  PacienteRepository pacienteRepository;

    @Autowired
    public PacienteService(PacienteRepository pacienteRepository) {
        this.pacienteRepository = pacienteRepository;
    }

    public List<Paciente> getPacientes() {
        return pacienteRepository.findAll();
    }

    public void addNewPaciente(Paciente paciente) {
        Optional<Paciente> pacienteOptional = pacienteRepository.findPacienteByCedula(paciente.getCedula());
        if(pacienteOptional.isPresent()){
            throw new IllegalStateException("La cedula ya se encuentra registrada");
        }
        pacienteRepository.save(paciente);
    }

    public void updatePaciente(Paciente paciente, Long id) {

        Paciente p = pacienteRepository.findById(id).orElseThrow(() -> new IllegalStateException("Paciente no encontrado"));

        if( paciente.getNombre() != null &&
                paciente.getNombre().length() > 0 &&
                !Objects.equals(p.getNombre(), paciente.getNombre())
        ){
            p.setNombre(paciente.getNombre());
        }

        if( paciente.getApellido() != null &&
                paciente.getApellido().length() > 0 &&
                !Objects.equals(p.getApellido(), paciente.getApellido())
        ){
            p.setApellido(paciente.getApellido());
        }

        if( paciente.getEmail() != null &&
                paciente.getEmail().length() > 0 &&
                !Objects.equals(p.getEmail(), paciente.getEmail())
        ){
            p.setEmail(paciente.getEmail());
        }

        if( paciente.getTelefono() != null &&
                paciente.getTelefono().length() > 0 &&
                !Objects.equals(p.getTelefono(), paciente.getTelefono())
        ){
            p.setTelefono(paciente.getTelefono());
        }

        if( paciente.getFechaNacimiento() != null &&
                !Objects.equals(p.getFechaNacimiento(), paciente.getFechaNacimiento())
        ){
            p.setFechaNacimiento(paciente.getFechaNacimiento());
        }

        pacienteRepository.save(p);
    }

    public void deleteStudent(Long id) {
        boolean exists = pacienteRepository.existsById(id);
        if(!exists){
            throw new IllegalStateException("El paciente no existe");
        }
        pacienteRepository.deleteById(id);
    }
}
