package com.pwbe.tpfinal2.ficha;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(path="api/v1/ficha")
public class FichaController {

    private final FichaService fichaService;

    @Autowired
    public FichaController(FichaService fichaService) {
        this.fichaService = fichaService;
    }

    @GetMapping
    public List<Ficha> getFichas(
            @RequestParam(required = false) String medico,
            @RequestParam(required = false) String especialidad,
            @RequestParam(required = false) String paciente,
            @RequestParam(required = false) String tratamiento,
            @RequestParam(required = false) String diagnostico,
            @RequestParam(required = false) String motivo,
            @RequestParam(required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            LocalDate fecha
    ) {
        if(
            medico != null ||
            especialidad != null ||
            paciente != null ||
            tratamiento != null ||
            diagnostico != null ||
            motivo != null ||
            fecha != null
        ){
            String mot = motivo != null? motivo: "";
            String diag = diagnostico != null? diagnostico: "";
            String trat = tratamiento != null? tratamiento: "";
            return fichaService.filterFichas(medico, especialidad, paciente, fecha, mot, diag, trat);
        }
        return fichaService.getFichas();
    }

    @PostMapping
    public void registerNewFicha(@RequestBody Ficha ficha){
        fichaService.addNewFicha(ficha);
    }
}
