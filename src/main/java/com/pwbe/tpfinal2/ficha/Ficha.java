package com.pwbe.tpfinal2.ficha;

import com.pwbe.tpfinal2.medico.Medico;
import com.pwbe.tpfinal2.paciente.Paciente;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class Ficha {

    @Id
    @SequenceGenerator(
            name = "ficha_sequence",
            sequenceName = "ficha_sequence",
            allocationSize = 1
    )

    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "ficha_sequence"
    )
    private Long id;
    private LocalDate fecha;
    private String motivo;
    private String diagnostico;
    private String tratamiento;

    @ManyToOne
    @JoinColumn(name="paciente_id", nullable=false)
    private Paciente paciente;

    @ManyToOne
    @JoinColumn(name="medico_id", nullable=false)
    private Medico medico;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Ficha(){}

    public Ficha(LocalDate fecha, String motivo, String diagnostico, String tratamiento, Paciente paciente, Medico medico) {
        this.fecha = fecha;
        this.motivo = motivo;
        this.diagnostico = diagnostico;
        this.tratamiento = tratamiento;
        this.paciente = paciente;
        this.medico = medico;
    }
}
