package com.pwbe.tpfinal2.ficha;

import com.pwbe.tpfinal2.medico.Medico;
import com.pwbe.tpfinal2.medico.MedicoRepository;
import com.pwbe.tpfinal2.paciente.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class FichaService {

    private final FichaRepository fichaRepository;

    @Autowired
    public FichaService(FichaRepository fichaRepository) {
        this.fichaRepository = fichaRepository;
    }

    public List<Ficha> getFichas() {
        return fichaRepository.findAll();
    }

    public List<Ficha> filterFichas(
            String medico,
            String especialidad,
            String paciente,
            LocalDate fecha,
            String motivo,
            String diagnostico,
            String tratamiento
    ) {
        return fichaRepository.findByMedico_cedulaOrMedico_especialidadOrPaciente_cedulaOrFechaOrMotivoLikeOrDiagnosticoLikeOrTratamientoLike(
                medico,
                especialidad,
                paciente,
                fecha,
                motivo,
                diagnostico,
                tratamiento
        );
    }

    public void addNewFicha(Ficha ficha) {
        fichaRepository.save(ficha);
    }

}
