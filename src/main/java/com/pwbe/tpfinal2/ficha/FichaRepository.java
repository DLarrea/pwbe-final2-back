package com.pwbe.tpfinal2.ficha;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FichaRepository extends JpaRepository<Ficha, Long> {

    List<Ficha> findByMedico_cedulaOrMedico_especialidadOrPaciente_cedulaOrFechaOrMotivoLikeOrDiagnosticoLikeOrTratamientoLike(
            String medico, String especialidad, String Paciente, LocalDate fecha, String motivo, String Diagnostico, String tratamiento
    );
}
